#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


#
# The following is a dummy URI that does not connect to a valid database. You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@35.227.79.146/proj1part2
#
# For example, if you had username gravano and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://gravano:foobar@35.227.79.146/proj1part2"
#
DATABASEURI = "postgresql://ia2380:7670@35.227.79.146/proj1part2"


#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)

#
# Example of running queries in your database
# Note that this will probably not work if you already have a table named 'test' in your database, containing meaningful data. This is only an example showing you how to run queries in your database using SQLAlchemy.
#
engine.execute("""CREATE TABLE IF NOT EXISTS test (
  id serial,
  name text
);""")
engine.execute("""INSERT INTO test(name) VALUES ('grace hopper'), ('alan turing'), ('ada lovelace');""")


@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()
  except:
    print "uh oh, problem connecting to database"
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't, the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


#
# @app.route is a decorator around index() that means:
#   run index() whenever the user tries to access the "/" path using a GET request
#
# If you wanted the user to go to, for example, localhost:8111/foobar/ with POST or GET then you could use:
#
#       @app.route("/foobar/", methods=["POST", "GET"])
#
# PROTIP: (the trailing / in the path is important)
# 
# see for routing: http://flask.pocoo.org/docs/0.10/quickstart/#routing
# see for decorators: http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
#
@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  # DEBUG: this is debugging code to see what request looks like
  print request.args


  #
  # example of a database query
  #the first table
  cursor = g.conn.execute("SELECT * FROM players_play_for_country ORDER BY name ASC")
  players = cursor.fetchall()
  cursor.close()
  #the second table
  cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid ORDER BY C.clubname ASC")
  clubplayedfor = cursor.fetchall()
  cursor.close()

  #
  # Flask uses Jinja templates, which is an extension to HTML where you can
  # pass data to a template and dynamically generate HTML based on the data
  # (you can think of it as simple PHP)
  # documentation: https://realpython.com/blog/python/primer-on-jinja-templating/
  #
  # You can see an example template in templates/index.html
  #
  # context are the variables that are passed to the template.
  # for example, "data" key in the context variable defined below will be 
  # accessible as a variable in index.html:
  #
  #     # will print: [u'grace hopper', u'alan turing', u'ada lovelace']
  #     <div>{{data}}</div>
  #     
  #     # creates a <div> tag for each element in data
  #     # will print: 
  #     #
  #     #   <div>grace hopper</div>
  #     #   <div>alan turing</div>
  #     #   <div>ada lovelace</div>
  #     #
  #     {% for n in data %}
  #     <div>{{n}}</div>
  #     {% endfor %}
  #
  context = dict(data = players, data2 = clubplayedfor)


  #
  # render_template looks in the templates/ folder for files.
  # for example, the below file reads template/index.html
  #
  return render_template("index.html", **context)

#
# This is an example of a different path.  You can see it at:
# 
#     localhost:8111/another
#
# Notice that the function name is another() rather than index()
# The functions for each app.route need to have different names
#
@app.route('/clubsmanagerstournaments')
def clubsmanagerstournaments():
  cursor = g.conn.execute("SELECT * FROM clubs_managed_by_managers ORDER BY clubname ASC")
  clubs = cursor.fetchall()
  cursor.close()
  cursor = g.conn.execute("SELECT * FROM tournaments ORDER BY size DESC")
  tournaments = cursor.fetchall()
  cursor.close()
  cursor = g.conn.execute("SELECT * FROM competes_in ORDER BY clubname ASC")
  competitors = cursor.fetchall()
  cursor.close()
  context = dict(data = clubs, data2 = tournaments, data3 = competitors)
  return render_template("clubsmanagerstournaments.html", **context)

#This is the path for National Teams
@app.route('/nationalteams')
def nationalteams():
  cursor = g.conn.execute("SELECT * FROM national_teams ORDER BY worldranking ASC")
  teams = cursor.fetchall()
  cursor.close()

  context = dict(data = teams)
  return render_template("nationalteams.html", **context)

# Example of adding new data to the database

@app.route('/add', methods=['POST'])
def add():
  name = request.form['name']
  g.conn.execute('INSERT INTO test(name) VALUES (%s)', name)
  return redirect('/')

#the reset button on the main page
@app.route('/reset')
def reset():
  print 'test'
  return redirect('/')

#the reset button on the clubs page
@app.route('/pg2reset')
def pg2reset():
  print 'test'
  return redirect('/clubsmanagerstournaments')

#the reset button on the national teams page
@app.route('/pg3reset')
def pg3reset():
  return redirect('/nationalteams')

#This route occurs when the filter button for the players table is clicked
#The result will return the table with the according filters applied to the table.
#The 'Club Played For' table will remain unchanged or revert to default if already filtered
@app.route('/playerfilter', methods=['POST'])
def playerfilter():

  height1 = request.form['height']
  weight1 = request.form['weight']
  heightop = request.form['heightop']
  weightop = request.form['weightop']

  if height1 == "" or weight1 == "":
    return redirect('/')

  height1 = float(height1)
  weight1 = float(weight1)

  if heightop == "Greater" and weightop == "Greater":
    cursor = g.conn.execute("SELECT * FROM players_play_for_country WHERE height >= %f and weight >= %f ORDER BY name ASC" % (height1, weight1))
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)
  elif heightop == "Greater" and weightop == "Less":
    cursor = g.conn.execute("SELECT * FROM players_play_for_country WHERE height >= %f and weight <= %f ORDER BY name ASC" % (height1, weight1))
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)
  elif heightop == "Less" and weightop == "Greater":
    cursor = g.conn.execute("SELECT * FROM players_play_for_country WHERE height <= %f and weight >= %f ORDER BY name ASC" % (height1, weight1))
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)
  elif heightop == "Less" and weightop == "Less":
    cursor = g.conn.execute("SELECT * FROM players_play_for_country WHERE height <= %f and weight <= %f ORDER BY name ASC" % (height1, weight1))
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)

  

#This is the filter button for the "Club Played For" table
@app.route('/playsforfilter', methods = ['POST'])
def playsforfilter():
  contracttype = request.form['contracttype']
  print 'test'
  if contracttype == 'Owned':
    cursor = g.conn.execute("SELECT * FROM players_play_for_country ORDER BY name ASC")
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid AND C.contracttype = 'Owned' ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)
  else:
    cursor = g.conn.execute("SELECT * FROM players_play_for_country ORDER BY name ASC")
    players = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT P.name, C.clubname, C.from1, C.to1, C.contracttype FROM players_play_for_country P, plays_for C WHERE C.playerid = P.playerid AND C.contracttype = 'Loan' ORDER BY C.clubname ASC")
    clubplayedfor = cursor.fetchall()
    cursor.close()
    context = dict(data = players, data2 = clubplayedfor)
    return render_template("index.html", **context)

@app.route('/titleordering', methods = ['POST'])
def titleordering():
  titleorder = request.form['titleorder']
  if titleorder == "Ascending":
    cursor = g.conn.execute("SELECT * FROM clubs_managed_by_managers ORDER BY domestictitles ASC")
    clubs = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM tournaments ORDER BY size DESC")
    tournaments = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM competes_in ORDER BY clubname ASC")
    competitors = cursor.fetchall()
    cursor.close()
    context = dict(data = clubs, data2 = tournaments, data3 = competitors)
    return render_template("clubsmanagerstournaments.html", **context)
  elif titleorder == "Descending":
    cursor = g.conn.execute("SELECT * FROM clubs_managed_by_managers ORDER BY domestictitles DESC")
    clubs = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM tournaments ORDER BY size DESC")
    tournaments = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM competes_in ORDER BY clubname ASC")
    competitors = cursor.fetchall()
    cursor.close()
    context = dict(data = clubs, data2 = tournaments, data3 = competitors)
    return render_template("clubsmanagerstournaments.html", **context)

@app.route('/winsordering', methods = ['POST'])
def winsordering():
  winsorder = request.form['winsorder']
  if winsorder == "Ascending":
    cursor = g.conn.execute("SELECT * FROM national_teams ORDER BY worldcupwins ASC")
    teams = cursor.fetchall()
    cursor.close()
    context = dict(data = teams)
    return render_template("nationalteams.html", **context)
  elif winsorder == "Descending":
    cursor = g.conn.execute("SELECT * FROM national_teams ORDER BY worldcupwins DESC")
    teams = cursor.fetchall()
    cursor.close()
    context = dict(data = teams)
    return render_template("nationalteams.html", **context)

@app.route('/winsordering2', methods = ['POST'])
def winsordering2():
  winsorder = request.form['winsorder2']
  if winsorder == "Ascending":
    cursor = g.conn.execute("SELECT * FROM clubs_managed_by_managers ORDER BY clubname ASC")
    clubs = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM tournaments ORDER BY size DESC")
    tournaments = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM competes_in ORDER BY wins ASC")
    competitors = cursor.fetchall()
    cursor.close()
    context = dict(data = clubs, data2 = tournaments, data3 = competitors)
    return render_template("clubsmanagerstournaments.html", **context)
  elif winsorder == "Descending":
    cursor = g.conn.execute("SELECT * FROM clubs_managed_by_managers ORDER BY clubname ASC")
    clubs = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM tournaments ORDER BY size DESC")
    tournaments = cursor.fetchall()
    cursor.close()
    cursor = g.conn.execute("SELECT * FROM competes_in ORDER BY wins DESC")
    competitors = cursor.fetchall()
    cursor.close()
    context = dict(data = clubs, data2 = tournaments, data3 = competitors)
    return render_template("clubsmanagerstournaments.html", **context)

@app.route('/login')
def login():
    abort(401)
    this_is_never_executed()


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using:

        python server.py

    Show the help text using:

        python server.py --help

    """

    HOST, PORT = host, port
    print "running on %s:%d" % (HOST, PORT)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
